package Business;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DeliveryService implements Serializable {
    private static HashSet<User> ask = new HashSet<User>();
    private static HashSet<MenuItem> ask2 = new HashSet<>();
    private static HashSet<Order> asdk3 = new HashSet<>();

    public static void addUs(User asd)
    {
        assert asd!=null;
        ask.add(asd);
    }
    public static HashSet<User> retUser()
    {

        assert ask!=null;
        return ask;
    }
    public static void addFoo(MenuItem foo){

        assert foo!=null;
        ask2.add(foo);
    }
    public static void setAsk(HashSet<User> askf){
        assert askf!=null;
        ask.addAll(askf);}
    public static void setAsk2(HashSet<MenuItem> askf){ask2.addAll(askf);}
    public static void setAsk3(HashSet<Order> ask3){asdk3.addAll(ask3);}


    public static void editFoo(MenuItem foo)
    {
        assert foo!=null;
        for(MenuItem mv: ask2)
        {
            if(Validat.eq(mv.getTitle(),foo.getTitle())==true)
            {
                mv.setRating(foo.getRating());
                mv.setCalories(foo.getCalories());
                mv.setProteins(foo.getProteins());
                mv.setFats(foo.getFats());
                mv.setSodium(foo.getSodium());
                mv.setPrice(foo.getPrice());
            }
        }
    }
    public static void delFoo(String fooName)
    {
        for(MenuItem mv: ask2)
        {
            if(Validat.eq(mv.getTitle(),fooName)==true)
            {
                ask2.remove(mv);
            }
        }
    }
    public static MenuItem lookForFoo(String fooName)
    {
        assert fooName!="";
        for(MenuItem s: ask2)
        {
            if(Validat.eq(s.getTitle(),fooName)==true)
            {
                return s;
            }
        }
        return null;
    }
    public static HashSet<MenuItem> getFoo()
    {
        assert ask2!=null;
        return ask2;
    }
    public static HashSet<Order> getAsdk3(){return asdk3;}

    public static List<MenuItem> shFoo(MenuItem foo)
    {
        List<MenuItem> shF = ask2.stream()
                .filter(c->c.getTitle().equals(foo.getTitle()))
                .filter(c->c.getRating() == foo.getRating())
                .filter(c->c.getCalories() == foo.getCalories())
                .filter(c->c.getProteins() == foo.getProteins())
                .filter(c->c.getFats() == foo.getFats())
                .filter(c->c.getSodium() == foo.getSodium())
                .filter(c->c.getPrice() == foo.getPrice())
                .collect(Collectors.toList());

        assert shF!=null;
        return shF;
    }
    public static boolean invar()
    {
        return ask!=null;
    }
    public static void addOrd(Order ord)
    {
        asdk3.add(ord);
    }

    public static void impFoo(String f)
    {
        assert f!="";
        try (Stream<String> lines = Files.lines(Paths.get(f))) {
            List<List<String>> values = lines.map(line -> Arrays.asList(line.split(","))).collect(Collectors.toList());
            for(int i=1;i<values.size();i++)
            {
                ask2.add(new BaseProduct(values.get(i).get(0),Double.parseDouble(values.get(i).get(1)),
                        Integer.parseInt(values.get(i).get(2)),Integer.parseInt(values.get(i).get(3)),
                        Integer.parseInt(values.get(i).get(4)),Integer.parseInt(values.get(i).get(5)),Integer.parseInt(values.get(i).get(6))));
            }
        } catch (IOException e) {
            e.printStackTrace();
    }

    }
}
