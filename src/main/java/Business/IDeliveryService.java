package Business;

import java.util.HashSet;
import java.util.List;

public interface IDeliveryService {

    /**
     * @pre asd!=null
     * @param asd
     */
    void addUs(User asd);

    /**
     * @post users!=null
     * @return
     */
    HashSet<User> retUser();

    /**
     * @pre foo != null
     * @param foo
     */
    void addFoo(MenuItem foo);

    /**
     * @pre askf!=null
     * @param askf
     */
    void setAsk(HashSet<User> askf);
    void setAsk2(HashSet<MenuItem> askf);
    void setAsk3(HashSet<Order> ask3);
    void editFoo(MenuItem foo);
    void delFoo(String fooName);

    /**
     * @post ask2 != null
     * @return
     */
    HashSet<MenuItem> getFoo();

    /**
     * @post ask3 != null
     * @return
     */
    HashSet<Order> getAsdk3();
    List<MenuItem> shFoo(MenuItem foo);

    /**
     * @pre ord!=null
     * @param ord
     */
    void addOrd(Order ord);

    /**
     * @pre f!=""
     * @param f
     */
    void impFoo(String f);




}
