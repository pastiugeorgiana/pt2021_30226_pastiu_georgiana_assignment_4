package Business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MenuItem implements Serializable {
    private String title;
    private double rating;
    private int calories;
    private int proteins;
    private int fats;
    private int sodium;
    private int price;
    private int jdf;
    private List<BaseProduct> asd = new ArrayList<>();
    private int menContor = 0;

    public MenuItem(String title,double rating,int calories,int proteins,int fats,int sodium,int price)
    {
        this.jdf = 0;
        this.title=title;
        this.rating=rating;
        this.calories=calories;
        this.proteins=proteins;
        this.fats=fats;
        this.sodium=sodium;
        this.price=price;
    }
    public void jdf()
    {
        this.jdf++;
    }
    public int getJdf()
    {
        return this.jdf;
    }

    public MenuItem(List<BaseProduct> asd,int price)
    {
        menContor++;
        this.title = "Meniu "+menContor;
        this.asd.addAll(asd);
        for(BaseProduct z: this.asd)
        {
            this.rating = this.rating + z.getRating();
            this.calories = this.calories + z.getCalories();
            this.proteins = this.proteins + z.getProteins();
            this.fats = this.fats + z.getFats();
            this.sodium = this.sodium + z.getSodium();
        }
        this.rating = this.rating / this.asd.size();
        this.price=price;
    }
    public String getTitle()
    {
        return this.title;
    }

    public double getRating() {
        return rating;
    }

    public int getCalories() {
        return calories;
    }

    public int getFats() {
        return fats;
    }

    public int getProteins() {
        return proteins;
    }

    public int getSodium() {
        return sodium;
    }

    public int getPrice() {
        return price;
    }

    public List<BaseProduct> getAsd() {
        return asd;
    }

    public void setAsd(List<BaseProduct> asd) {
        this.asd = asd;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public void setFats(int fats) {
        this.fats = fats;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setProteins(int proteins) {
        this.proteins = proteins;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setSodium(int sodium) {
        this.sodium = sodium;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuItem menuItem = (MenuItem) o;
        return Objects.equals(title, menuItem.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}
