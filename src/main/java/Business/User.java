package Business;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {
    private int id;
    private String name;
    private String pass;
    private String att;
    private int yu = 0;
    private int yu2 = 0;

    public User(int id,String name,String pass,String att)
    {
        this.id=id;
        this.name=name;
        this.pass=pass;
        this.att=att;
    }
    public int getId()
    {
        return this.id;
    }
    public String getName()
    {
        return this.name;
    }
    public String getPass()
    {
        return this.pass;
    }
    public void setId(int asd)
    {
        this.id=asd;
    }
    public void setName(String asd)
    {
        this.name=asd;
    }
    public void setPass(String asd)
    {
        this.pass=asd;
    }
    public void setAtt(String att)
    {
        this.att = att;
    }
    public String getAtt()
    {
        return this.att;
    }
    public void ble(){this.yu++;}
    public void ble2(int sar){this.yu2+=sar;}

    public int getYu() {
        return yu;
    }

    public int getYu2() {
        return yu2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
