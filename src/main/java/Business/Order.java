package Business;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Order implements Serializable {

    private int id;
    private int price;
    private LocalDateTime am;
    private ArrayList<MenuItem> orderFood = new ArrayList<>();

    public Order(int id,ArrayList<MenuItem> orderFood)
    {
        this.id = id;
        this.orderFood.addAll(orderFood);
        this.am = LocalDateTime.now();

        for(MenuItem g: this.orderFood)
        {
            this.price += g.getPrice();
        }
    }

    public int takeID()
    {
        return this.id;
    }
    public int takePrice()
    {
        return this.price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id;
    }
    public List<MenuItem> gf(){return this.orderFood;}

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
    public LocalDateTime getDa(){return this.am;}

}
