package Business;

import Presentation.Inter4;

import java.util.ArrayList;
import java.util.Observable;

public class Observer implements java.util.Observer {
    private static Inter4 inter4;

    public Observer(ArrayList<Order> ls)
    {
        inter4 = new Inter4(ls);
    }

    @Override
    public void update(Observable o, Object arg) {
        inter4.dispose();
        inter4 = new Inter4(Manager.getOr());
    }

    public static Inter4 getSd()
    {
        return inter4;
    }
}
