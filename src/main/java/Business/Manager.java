package Business;

import Data.Serial;
import Data.Write;
import Presentation.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.stream.Collectors;

public class Manager extends Observable {
    private Inter1 ye;
    private int idUser = 0;
    private int idOrder = 0;
    private int pri = 0;
    private Write hg  = new Write();
    private List<BaseProduct> lv = new ArrayList<>();
    private List<MenuItem> lc = new ArrayList<>();
    private static ArrayList<Order> sd = new ArrayList<>();
    private User us ;

    public Manager(Inter1 ye)
    {
        try {
            Serial.des("C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\da.txt","C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\da2.txt","C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\da3.txt");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        this.ye=ye;
        this.ye.setVisible(true);

        this.ye.getCrate().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int se = Validat.verify(ye.getNameC(),ye.getPasC());

                if(se == -1)
                {
                    try {
                        throw new Exc("Something is wrong");
                    } catch (Exc exc) {
                        exc.printStackTrace();
                    }

                }else
                {
                    idUser++;
                    DeliveryService.addUs(new User(idUser,ye.getNameC(),ye.getPasC(),ye.getT()));
                    System.out.println("User create "+ye.getNameC()+" "+ye.getPasC()+" "+ye.getT());
                }
            }
        });
        this.ye.getLog().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                User lk = Validat.ver2(ye.getNameLog(),ye.getPasLog());
                us = lk;
                if(lk == null)
                {
                    try {
                        throw new Exc("Something is wrong");
                    } catch (Exc exc) {
                        exc.printStackTrace();
                    }
                }
                else
                {
                    if(lk.getAtt().equals("Administrator") == true)
                    {
                        Inter3 asd = new Inter3();
                        asd.setVisible(true);
                        ye.setVisible(false);

                        asd.getButBack().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                asd.dispose();
                                ye.setVisible(true);
                            }
                        });
                        asd.getButAdd().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                String kl = asd.getBaseName();
                                double kl2 = asd.getBaseRating();
                                int kl3 = asd.getBaseCalories();
                                int kl4 = asd.getBaseProtein();
                                int kl5 = asd.getBaseFats();
                                int kl6 = asd.getBaseSodium();
                                int kl7 = asd.getBasePrice();

                                DeliveryService.addFoo(new BaseProduct(kl,kl2,kl3,kl4,kl5,kl6,kl7));
                            }
                        });
                        asd.getButEdit().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                String kl = asd.getEditName();
                                double kl2 = asd.getEditRating();
                                int kl3 = asd.getEditCalories();
                                int kl4 = asd.getEditProtein();
                                int kl5 = asd.getEditFat();
                                int kl6 = asd.getEditSodium();
                                int kl7 = asd.getEditPrice();

                                DeliveryService.editFoo(new MenuItem(kl,kl2,kl3,kl4,kl5,kl6,kl7));
                            }
                        });
                        asd.getButDelete().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                String kl =  asd.getDeleteName();
                                DeliveryService.delFoo(kl);
                            }
                        });
                        asd.getCompusAdd().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                String lk = asd.getCompusName();
                                lv.add((BaseProduct) DeliveryService.lookForFoo(lk));
                                pri += DeliveryService.lookForFoo(lk).getPrice();
                            }
                        });
                        asd.getCompusMake().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                DeliveryService.addFoo(new MenuItem(lv,pri));
                                pri = 0;
                                lv.clear();
                            }
                        });
                        asd.getButImport().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                DeliveryService.impFoo("C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\products.CSV");
                            }
                        });
                        asd.getRap1().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                List<Order> lg = DeliveryService.getAsdk3().stream()
                                        .filter(s->s.getDa().getHour()>=asd.getStartH() && s.getDa().getHour()<=asd.getEndH())
                                        .collect(Collectors.toList());
                                if(hg.r1("C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\1.txt",lg)==false)
                                    try {
                                        throw new Exc("Something is wrong");
                                    } catch (Exc exc) {
                                        exc.printStackTrace();
                                    }
                            }
                        });
                        asd.getRap2().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                             List<MenuItem> lf = DeliveryService.getFoo().stream()
                                     .filter(s->s.getJdf()>asd.getRap2Value())
                                     .collect(Collectors.toList());

                             if(hg.r2("C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\2.txt",lf)==false)
                             {
                                 try {
                                     throw new Exc("Something is wrong");
                                 } catch (Exc exc) {
                                     exc.printStackTrace();
                                 }
                             }
                            }
                        });
                        asd.getRap4().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                List<Order> bla = DeliveryService.getAsdk3().stream()
                                        .filter(s->s.getDa().getDayOfMonth() == Integer.parseInt(asd.getRap4Value()))
                                        .collect(Collectors.toList());

                                HashSet<MenuItem> bla2 = new HashSet<>();

                                for(int i = 0;i<bla.size();i++)
                                {
                                    bla2.addAll(bla.get(i).gf());
                                }

                                List<MenuItem> bla3 = new ArrayList<>();

                                bla3.addAll(bla2);

                                if(hg.r4("C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\4.txt",bla3) == false)
                                {
                                    try {
                                        throw new Exc("Something is wrong");
                                    } catch (Exc exc) {
                                        exc.printStackTrace();
                                    }
                                }

                            }
                        });
                        asd.getRap3().addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                int un = asd.getRap3Value();
                                int du = asd.getRap31Value();

                                List<User> lsb = DeliveryService.retUser().stream()
                                        .filter(s->s.getYu()>=un && s.getYu2()>=du)
                                        .collect(Collectors.toList());

                                if(hg.r3("C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\3.txt",lsb)==false)
                                {
                                    try {
                                        throw new Exc("Something is wrong");
                                    } catch (Exc exc) {
                                        exc.printStackTrace();
                                    }
                                }
                            }
                        });

                    }else
                    {
                        if(lk.getAtt().equals("Client")==true) {
                            Inter2 asd = new Inter2();
                            asd.setVisible(true);
                            ye.setVisible(false);

                            asd.getBack().addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    asd.dispose();
                                    ye.setVisible(true);
                                }
                            });
                            asd.getPrint().addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                   ArrayList<MenuItem> lds = new ArrayList<>();
                                   lds.addAll(DeliveryService.getFoo());
                                   Inter5 inter5 =  new Inter5(lds);
                                    inter5.setVisible(true);
                                    asd.setVisible(false);

                                    inter5.getBackBtn().addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            inter5.dispose();
                                            asd.setVisible(true);
                                        }
                                    });
                                }
                            });
                            asd.getSearch().addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    String lk = asd.getSearchName();
                                    double lk2 = asd.getRatingSearch();
                                    int lk3 = asd.getCaloriesSearch();
                                    int lk4 = asd.getProteinSearch();
                                    int lk5 = asd.getFatsSearch();
                                    int lk6 = asd.getSodiumSearch();
                                    int lk7 = asd.getPriceSearch();

                                    Inter5 inter5 = new Inter5((ArrayList<MenuItem>) DeliveryService.shFoo(new MenuItem(lk,lk2,lk3,lk4,lk5,lk6,lk7)));
                                    inter5.setVisible(true);
                                    asd.setVisible(false);

                                    inter5.getBackBtn().addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            inter5.dispose();
                                            asd.setVisible(true);
                                        }
                                    });

                                }
                            });
                            asd.getAddInOrder().addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    String lk = asd.getOrderName();
                                    lc.add(DeliveryService.lookForFoo(lk));
                                    DeliveryService.lookForFoo(lk).jdf();
                                }
                            });
                            asd.getOrder().addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    idOrder++;
                                    us.ble();
                                    int pr = 0;
                                    for(int i=0;i<lc.size();i++)
                                        pr+= lc.get(i).getPrice();

                                    us.ble2(pr);
                                    Order vc = new Order(idOrder, (ArrayList<MenuItem>) lc);
                                    sd.add(vc);
                                    DeliveryService.addOrd(vc);
                                    setChanged();
                                    notifyObservers();
                                    lc.clear();
                                    if(hg.write("C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\bill.txt",vc)==false)
                                    {
                                        try {
                                            throw new Exc("Something is wrong");
                                        } catch (Exc exc) {
                                            exc.printStackTrace();
                                        }
                                    }
                                }
                            });
                        }else
                        {
                            if(lk.getAtt().equals("Employee")==true)
                            {
                                Inter4 asd = Observer.getSd();
                                asd.setVisible(true);
                                ye.setVisible(false);

                                asd.getBackBtn().addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        asd.dispose();
                                        ye.setVisible(true);
                                    }
                                });


                            }
                            else
                            {
                                try {
                                    throw new Exc("Something is wrong");
                                } catch (Exc exc) {
                                    exc.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        });
        this.ye.getJ().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    Serial.seri("C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\da.txt","C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\da2.txt","C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\da3.txt");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });
    }
    public static ArrayList<Order> getOr()
    {
        return sd;
    }
}
