package Presentation;

import Business.MenuItem;
import Business.Order;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;

public class Inter4 extends JFrame {

    private final JButton backBtn;


    public Inter4(ArrayList<Order> lds) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 280, 440);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        ImagePanel panelz = new ImagePanel(
                new ImageIcon("C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\img3.jpg").getImage());


        backBtn = new JButton("Back");
        backBtn.setFont(new Font("Times New Roman", Font.PLAIN, 15));
        backBtn.setBounds(95, 370, 85, 21);
        contentPane.add(backBtn);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 10, 250, 350);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);
        contentPane.add(scrollPane);



        String[] led = {"ID","Price"};
        Object[][] obj = new Object[lds.size()][led.length];


        int d = 0;

        for(Order i: lds)
        {
            obj[d][0] = i.takeID();
            obj[d][1] = i.takePrice();

            d++;
        }

        JTable post =  new JTable(obj,led);


        scrollPane.setViewportView(post);
        contentPane.add(panelz);
    }

    public JButton getBackBtn() {
        return this.backBtn;
    }
}
