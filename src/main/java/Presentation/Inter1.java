package Presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class Inter1 extends JFrame {

    private JPanel contentPane;
    private JTextField xc1;
    private JTextField xc2;
    private JTextField xc3;
    private JTextField xc4;
    private JButton log;
    private JButton crate;
    private JComboBox comboBox;

    public Inter1() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 714, 260);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        Panel as = new Panel();
        as.setBackground(Color.GRAY);
        as.setBounds(280, 10, 157, 203);
        contentPane.add(as);
        as.setLayout(null);

        JLabel asd1 = new JLabel("Name:");
        asd1.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd1.setForeground(Color.WHITE);
        asd1.setBounds(55, 10, 45, 15);
        as.add(asd1);

        xc1 = new JTextField();
        xc1.setBounds(18, 35, 120, 20);
        as.add(xc1);
        xc1.setColumns(10);

        JLabel asd2 = new JLabel("Pass:");
        asd2.setForeground(Color.WHITE);
        asd2.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd2.setBounds(60, 65, 45, 15);
        as.add(asd2);

        xc2 = new JPasswordField();
        xc2.setColumns(10);
        xc2.setBounds(18, 90, 120, 20);
        as.add(xc2);

        log = new JButton("Login");
        log.setFont(new Font("Tahoma", Font.PLAIN, 15));
        log.setBounds(37, 172, 85, 21);
        as.add(log);

        Panel as2 = new Panel();
        as2.setLayout(null);
        as2.setBackground(Color.GRAY);
        as2.setBounds(445, 10, 157, 203);
        contentPane.add(as2);

        JLabel asd3 = new JLabel("Name:");
        asd3.setForeground(Color.WHITE);
        asd3.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd3.setBounds(55, 10, 45, 15);
        as2.add(asd3);

        xc3 = new JTextField();
        xc3.setColumns(10);
        xc3.setBounds(18, 35, 120, 20);
        as2.add(xc3);

        JLabel asd4 = new JLabel("Pass:");
        asd4.setForeground(Color.WHITE);
        asd4.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd4.setBounds(60, 65, 45, 15);
        as2.add(asd4);

        xc4 = new JPasswordField();
        xc4.setColumns(10);
        xc4.setBounds(18, 90, 120, 20);
        as2.add(xc4);

        crate = new JButton("Create");
        crate.setFont(new Font("Tahoma", Font.PLAIN, 15));
        crate.setBounds(37, 172, 85, 21);
        as2.add(crate);

        String[] soap = {"Administrator","Employee","Client"};

        comboBox = new JComboBox(soap);
        comboBox.setBounds(18, 124, 120, 21);
        as2.add(comboBox);

        ImagePanel panel = new ImagePanel(
        new ImageIcon("C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\img4.jpg").getImage());

        contentPane.add(as);
        contentPane.add(as2);
        contentPane.add(panel);
    }
    public JButton getLog()
    {
        return this.log;
    }
    public JButton getCrate()
    {
        return this.crate;
    }
    public String getNameLog()
    {
        return this.xc1.getText();
    }
    public String getPasLog()
    {
        return this.xc2.getText();
    }
    public String getNameC()
    {
        return this.xc3.getText();
    }
    public String getPasC()
    {
        return this.xc4.getText();
    }
    public String getT()
    {
        return this.comboBox.getSelectedItem().toString();
    }
    public JFrame getJ(){return this;};

}
