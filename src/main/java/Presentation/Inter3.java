package Presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;

public class Inter3 extends JFrame {

    private JPanel contentPane;
    private JTextField baseName;
    private JTextField baseRating;
    private JTextField baseCalories;
    private JTextField baseProtein;
    private JTextField baseFats;
    private JTextField baseSodium;
    private JTextField basePrice;
    private JTextField editName;
    private JTextField editRating;
    private JTextField editCalories;
    private JTextField editProtein;
    private JTextField editFats;
    private JTextField editSodium;
    private JTextField editPrice;
    private JTextField compusName;
    private JTextField deleteName;
    private JButton butAdd;
    private JButton butEdit;
    private JButton compusAdd;
    private JButton compusMake;
    private JButton butDelete;
    private JButton butBack;
    private JButton rap1;
    private JButton rap2;
    private JButton rap3;
    private JButton rap4;
    private JButton butImport;
    private JTextField rap1Field;
    private JTextField rap11Field;
    private JTextField rap2Field;
    private JTextField rap4Field;
    private JTextField rap3Field;
    private JTextField rap31Field;


    public Inter3() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 600, 620);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        ImagePanel panelz = new ImagePanel(
                new ImageIcon("C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\img5.jpg").getImage());

        JPanel panelC = new JPanel();
        panelC.setBorder(new LineBorder(new Color(0, 0, 0)));
        panelC.setBounds(10, 10, 193, 420);
        panelC.setBackground(Color.GRAY);
        contentPane.add(panelC);
        panelC.setLayout(null);

        JLabel asd1 = new JLabel("Name");
        asd1.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd1.setBounds(74, 10, 45, 13);
        panelC.add(asd1);

        baseName = new JTextField();
        baseName.setBounds(20, 33, 150, 19);
        panelC.add(baseName);
        baseName.setColumns(10);

        JLabel asd2 = new JLabel("Rating");
        asd2.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd2.setBounds(74, 62, 45, 13);
        panelC.add(asd2);

        baseRating = new JTextField();
        baseRating.setColumns(10);
        baseRating.setBounds(20, 85, 150, 19);
        panelC.add(baseRating);

        JLabel asd3 = new JLabel("Calories");
        asd3.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd3.setBounds(70, 114, 60, 13);
        panelC.add(asd3);

        baseCalories = new JTextField();
        baseCalories.setColumns(10);
        baseCalories.setBounds(20, 137, 150, 19);
        panelC.add(baseCalories);

        JLabel asd4 = new JLabel("Protein");
        asd4.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd4.setBounds(74, 166, 60, 13);
        panelC.add(asd4);

        baseProtein = new JTextField();
        baseProtein.setColumns(10);
        baseProtein.setBounds(20, 189, 150, 19);
        panelC.add(baseProtein);

        JLabel asd5 = new JLabel("Fats");
        asd5.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd5.setBounds(80, 218, 45, 13);
        panelC.add(asd5);

        baseFats = new JTextField();
        baseFats.setColumns(10);
        baseFats.setBounds(20, 241, 150, 19);
        panelC.add(baseFats);

        JLabel asd6 = new JLabel("Sodium");
        asd6.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd6.setBounds(72, 270, 70, 13);
        panelC.add(asd6);

        baseSodium = new JTextField();
        baseSodium.setColumns(10);
        baseSodium.setBounds(20, 293, 150, 19);
        panelC.add(baseSodium);

        JLabel asd7 = new JLabel("Price");
        asd7.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd7.setBounds(78, 322, 70, 13);
        panelC.add(asd7);

        basePrice = new JTextField();
        basePrice.setColumns(10);
        basePrice.setBounds(20, 345, 150, 19);
        panelC.add(basePrice);

        butAdd = new JButton("Add");
        butAdd.setBounds(55, 389, 85, 21);
        panelC.add(butAdd);

        JPanel panelA = new JPanel();
        panelA.setLayout(null);
        panelA.setBorder(new LineBorder(new Color(0, 0, 0)));
        panelA.setBounds(213, 10, 193, 420);
        panelA.setBackground(Color.GRAY);
        contentPane.add(panelA);

        JLabel asd8 = new JLabel("Name");
        asd8.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd8.setBounds(74, 10, 45, 13);
        panelA.add(asd8);

        editName = new JTextField();
        editName.setColumns(10);
        editName.setBounds(20, 33, 150, 19);
        panelA.add(editName);

        JLabel asd9 = new JLabel("Rating");
        asd9.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd9.setBounds(74, 62, 45, 13);
        panelA.add(asd9);

        editRating = new JTextField();
        editRating.setColumns(10);
        editRating.setBounds(20, 85, 150, 19);
        panelA.add(editRating);

        JLabel asd10 = new JLabel("Calories");
        asd10.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd10.setBounds(70, 114, 60, 13);
        panelA.add(asd10);

        editCalories = new JTextField();
        editCalories.setColumns(10);
        editCalories.setBounds(20, 137, 150, 19);
        panelA.add(editCalories);

        JLabel asd11 = new JLabel("Protein");
        asd11.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd11.setBounds(74, 166, 60, 13);
        panelA.add(asd11);

        editProtein = new JTextField();
        editProtein.setColumns(10);
        editProtein.setBounds(20, 189, 150, 19);
        panelA.add(editProtein);

        JLabel asd12 = new JLabel("Fats");
        asd12.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd12.setBounds(80, 218, 45, 13);
        panelA.add(asd12);

        editFats = new JTextField();
        editFats.setColumns(10);
        editFats.setBounds(20, 241, 150, 19);
        panelA.add(editFats);

        JLabel asd13 = new JLabel("Sodium");
        asd13.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd13.setBounds(72, 270, 70, 13);
        panelA.add(asd13);

        editSodium = new JTextField();
        editSodium.setColumns(10);
        editSodium.setBounds(20, 293, 150, 19);
        panelA.add(editSodium);

        JLabel asd14 = new JLabel("Price");
        asd14.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd14.setBounds(78, 322, 70, 13);
        panelA.add(asd14);

        editPrice = new JTextField();
        editPrice.setColumns(10);
        editPrice.setBounds(20, 345, 150, 19);
        panelA.add(editPrice);

        butEdit = new JButton("Edit");
        butEdit.setBounds(55, 389, 85, 21);
        panelA.add(butEdit);

        JPanel panelB = new JPanel();
        panelB.setBorder(new LineBorder(new Color(0, 0, 0)));
        panelB.setBounds(416, 10, 160, 420);
        panelB.setBackground(Color.gray);
        contentPane.add(panelB);
        panelB.setLayout(null);

        JLabel asd15 = new JLabel("Name");
        asd15.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd15.setBounds(56, 10, 50, 13);
        panelB.add(asd15);

        compusName = new JTextField();
        compusName.setBounds(10, 33, 140, 19);
        panelB.add(compusName);
        compusName.setColumns(10);

        compusAdd = new JButton("Add");
        compusAdd.setBounds(38, 62, 85, 21);
        panelB.add(compusAdd);

        compusMake = new JButton("Make");
        compusMake.setBounds(38, 93, 85, 21);
        panelB.add(compusMake);

        JLabel asd16 = new JLabel("Name");
        asd16.setFont(new Font("Tahoma", Font.PLAIN, 15));
        asd16.setBounds(56, 182, 50, 13);
        panelB.add(asd16);

        deleteName = new JTextField();
        deleteName.setColumns(10);
        deleteName.setBounds(10, 205, 140, 19);
        panelB.add(deleteName);

        butDelete = new JButton("Delete");
        butDelete.setBounds(38, 234, 85, 21);
        panelB.add(butDelete);

        butBack = new JButton("Back");
        butBack.setBounds(38, 389, 85, 21);
        panelB.add(butBack);

        rap1 = new JButton("Raport 1");
        rap1.setBounds(38, 483, 85, 21);
        contentPane.add(rap1);

        rap2 = new JButton("Raport 2");
        rap2.setBounds(174, 483, 85, 21);
        contentPane.add(rap2);

        rap3 = new JButton("Raport 3");
        rap3.setBounds(317, 483, 85, 21);
        contentPane.add(rap3);

        rap4 = new JButton("Raport 4");
        rap4.setBounds(454, 483, 85, 21);
        contentPane.add(rap4);

        butImport = new JButton("Import");
        butImport.setBounds(246, 538, 85, 21);
        contentPane.add(butImport);


        rap1Field = new JTextField();
        rap1Field.setBounds(25, 515, 49, 20);
        contentPane.add(rap1Field);
        rap1Field.setColumns(10);

        rap11Field = new JTextField();
        rap11Field.setColumns(10);
        rap11Field.setBounds(90, 515, 49, 20);
        contentPane.add(rap11Field);

        rap2Field = new JTextField();
        rap2Field.setColumns(10);
        rap2Field.setBounds(194, 514, 49, 20);
        contentPane.add(rap2Field);

        rap4Field = new JTextField();
        rap4Field.setColumns(10);
        rap4Field.setBounds(475, 515, 49, 20);
        contentPane.add(rap4Field);

        rap3Field = new JTextField();
        rap3Field.setColumns(10);
        rap3Field.setBounds(303, 514, 49, 20);
        contentPane.add(rap3Field);

        rap31Field = new JTextField();
        rap31Field.setColumns(10);
        rap31Field.setBounds(362, 514, 49, 20);
        contentPane.add(rap31Field);

        contentPane.add(panelz);
    }

    public String getBaseName()
    {
        return this.baseName.getText();
    }
    public double getBaseRating()
    {
        return Double.parseDouble(this.baseRating.getText());
    }
    public int getBaseCalories()
    {
        return Integer.parseInt(this.baseCalories.getText());
    }
    public int getBaseProtein()
    {
        return Integer.parseInt(this.baseProtein.getText());
    }
    public int getBaseFats()
    {
        return Integer.parseInt(this.baseFats.getText());
    }
    public int getBaseSodium()
    {
        return Integer.parseInt(this.baseSodium.getText());
    }
    public int getBasePrice()
    {
        return Integer.parseInt(this.basePrice.getText());
    }

    public String getEditName()
    {
        return this.editName.getText();
    }
    public double getEditRating()
    {
        return Double.parseDouble(this.editRating.getText());
    }
    public int getEditCalories()
    {
        return Integer.parseInt(this.editCalories.getText());
    }
    public int getEditProtein()
    {
        return Integer.parseInt(this.editProtein.getText());
    }
    public int getEditFat()
    {
        return Integer.parseInt(this.editFats.getText());
    }
    public int getEditPrice()
    {
        return Integer.parseInt(this.editPrice.getText());
    }
    public int getEditSodium(){return Integer.parseInt(this.editSodium.getText());}
    public String getCompusName()
    {
        return this.compusName.getText();
    }
    public String getDeleteName()
    {
        return this.deleteName.getText();
    }
    public JButton getButAdd()
    {
        return this.butAdd;
    }
    public JButton getButEdit()
    {
        return this.butEdit;
    }
    public JButton getCompusAdd()
    {
        return this.compusAdd;
    }
    public JButton getCompusMake()
    {
        return this.compusMake;
    }
    public JButton getButDelete()
    {
        return this.butDelete;
    }
    public JButton getButBack()
    {
        return this.butBack;
    }
    public JButton getRap1()
    {
        return this.rap1;
    }
    public JButton getRap2()
    {
        return this.rap2;
    }

    public JButton getRap3() {
        return this.rap3;
    }

    public JButton getRap4()
    {
        return this.rap4;
    }
    public JButton getButImport()
    {
        return this.butImport;
    }

    public int getStartH()
    {
        return Integer.parseInt(this.rap1Field.getText());
    }
    public int getEndH()
    {
        return Integer.parseInt(this.rap11Field.getText());
    }

    public int getRap2Value()
    {
        return Integer.parseInt(this.rap2Field.getText());
    }
    public int getRap3Value()
    {
        return Integer.parseInt(this.rap3Field.getText());
    }
    public int getRap31Value()
    {
        return Integer.parseInt(this.rap31Field.getText());
    }
    public String getRap4Value()
    {
        return this.rap4Field.getText();
    }
}
