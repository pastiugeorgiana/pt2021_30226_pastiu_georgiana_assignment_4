package Presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class Inter2 extends JFrame {

    private JPanel contentPane;
    private JTextField order;
    private JTextField name;
    private JTextField rating;
    private JTextField calories;
    private JTextField protein;
    private JTextField fat;
    private JTextField sodium;
    private JTextField price;
    private JButton addFoodInOrder;
    private JButton getOrder;
    private JButton search;
    private JButton print;
    private JButton back;


    public Inter2() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 626, 417);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        ImagePanel panelz = new ImagePanel(
                new ImageIcon("C:\\Users\\Iuliana\\PT2021_30226_Pastiu_Georgiana_Assignment_4\\src\\main\\resources\\img0.jpeg").getImage());

        JPanel panelA = new JPanel();
        panelA.setBackground(Color.LIGHT_GRAY);
        panelA.setBounds(10, 34, 172, 315);
        contentPane.add(panelA);
        panelA.setLayout(null);

        JLabel asd1 = new JLabel("Food Name");
        asd1.setBounds(55, 90, 70, 15);
        panelA.add(asd1);

        order = new JTextField();
        order.setBounds(38, 110, 100, 20);
        panelA.add(order);
        order.setColumns(10);

        addFoodInOrder = new JButton("Add");
        addFoodInOrder.setBounds(43, 190, 85, 21);
        panelA.add(addFoodInOrder);

        getOrder = new JButton("Order");
        getOrder.setBounds(43, 220, 85, 21);
        panelA.add(getOrder);

        JPanel panelB = new JPanel();
        panelB.setLayout(null);
        panelB.setBackground(Color.LIGHT_GRAY);
        panelB.setBounds(430, 34, 172, 315);
        contentPane.add(panelB);

        JLabel asd2 = new JLabel("Food Name");
        asd2.setBounds(53, 10, 70, 15);
        panelB.add(asd2);

        name = new JTextField();
        name.setColumns(10);
        name.setBounds(35, 25, 100, 20);
        panelB.add(name);

        JLabel asd3 = new JLabel("Rating");
        asd3.setBounds(65, 47, 70, 15);
        panelB.add(asd3);

        rating = new JTextField();
        rating.setColumns(10);
        rating.setBounds(35, 65, 100, 20);
        panelB.add(rating);

        JLabel asd4 = new JLabel("Calories");
        asd4.setBounds(62, 90, 70, 15);
        panelB.add(asd4);

        calories = new JTextField();
        calories.setColumns(10);
        calories.setBounds(35, 110, 100, 20);
        panelB.add(calories);

        JLabel asd5 = new JLabel("Protein");
        asd5.setBounds(62, 130, 70, 15);
        panelB.add(asd5);

        protein = new JTextField();
        protein.setColumns(10);
        protein.setBounds(35, 150, 100, 20);
        panelB.add(protein);

        JLabel asd6 = new JLabel("Fats");
        asd6.setBounds(70, 170, 70, 15);
        panelB.add(asd6);

        fat = new JTextField();
        fat.setColumns(10);
        fat.setBounds(35, 185, 100, 20);
        panelB.add(fat);

        JLabel asd7 = new JLabel("Sodium");
        asd7.setBounds(64, 205, 70, 15);
        panelB.add(asd7);

        sodium = new JTextField();
        sodium.setColumns(10);
        sodium.setBounds(35, 220, 100, 20);
        panelB.add(sodium);

        JLabel asd8 = new JLabel("Price");
        asd8.setBounds(68, 240, 70, 15);
        panelB.add(asd8);

        price = new JTextField();
        price.setColumns(10);
        price.setBounds(35, 255, 100, 20);
        panelB.add(price);

        search = new JButton("Search");
        search.setBounds(42, 285, 85, 21);
        panelB.add(search);

        print = new JButton("Print");
        print.setBounds(258, 146, 85, 21);
        contentPane.add(print);

        back = new JButton("Back");
        back.setBounds(258, 191, 85, 21);
        contentPane.add(back);

        contentPane.add(panelz);
    }

    public String getSearchName()
    {
        return this.name.getText();
    }

    public String getOrderName()
    {
        return this.order.getText();
    }

    public double getRatingSearch()
    {
        return Double.parseDouble(this.rating.getText());
    }

    public int getCaloriesSearch()
    {
        return Integer.parseInt(this.calories.getText()) ;
    }

    public int getProteinSearch()
    {
        return Integer.parseInt(this.protein.getText());
    }

    public int getFatsSearch()
    {
        return Integer.parseInt(this.fat.getText());
    }
    public int getPriceSearch()
    {
        return Integer.parseInt(this.price.getText());
    }
    public int getSodiumSearch(){return Integer.parseInt(this.sodium.getText());}
    public JButton getAddInOrder()
    {
        return this.addFoodInOrder;
    }
    public JButton getOrder()
    {
        return this.getOrder;
    }
    public JButton getSearch()
    {
        return this.search;
    }
    public JButton getPrint()
    {
        return this.print;
    }
    public JButton getBack()
    {
        return this.back;
    }



}
