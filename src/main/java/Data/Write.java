package Data;

import Business.MenuItem;
import Business.Order;
import Business.User;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;

public class Write {

    public boolean write(String jf, Order s)
    {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(jf));
            out.write("Bill:\n");
            out.write("ID: "+s.takeID()+"\n");
            out.write("Price: "+s.takePrice()+"\n");
            out.write("Data: "+s.getDa().getDayOfMonth()+" "+s.getDa().getMonth());
            out.close();
            return true;
        }
        catch (IOException e) {
            return false;
        }
    }
    public boolean r1(String jf, List<Order> xc)
    {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(jf));
            for(int i=0;i<xc.size();i++)
            {
                out.write("ID: "+xc.get(i).takeID()+"\n"+"Price: "+xc.get(i).takePrice()+"\n"+"Hour: "+xc.get(i).getDa().getHour());
            }
            out.close();
            return true;
        }
        catch (IOException e) {
            return false;
        }
    }
    public boolean r2(String jf, List<MenuItem> xc)
    {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(jf));
            for(int i=0;i<xc.size();i++)
            {
                out.write(xc.get(i).getTitle()+"\n"+xc.get(i).getJdf());
            }
            out.close();
            return true;
        }
        catch (IOException e) {
            return false;
        }
    }
    public boolean r4(String jf,List<MenuItem> xc)
    {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(jf));
            for(int i=0;i<xc.size();i++)
            {
                out.write(xc.get(i).getTitle() + " Ordered "+xc.get(i).getJdf()+" times"+"\n");
            }
            out.close();
            return true;
        }
        catch (IOException e) {
            return false;
        }
    }

    public boolean r3(String jf,List<User> xc)
    {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(jf));
            for(int i=0;i<xc.size();i++)
            {
                out.write("Name: "+xc.get(i).getName()+" ord times: "+xc.get(i).getYu()+" sum: "+xc.get(i).getYu2()+"\n");
            }
            out.close();
            return true;
        }
        catch (IOException e) {
            return false;
        }
    }

}
