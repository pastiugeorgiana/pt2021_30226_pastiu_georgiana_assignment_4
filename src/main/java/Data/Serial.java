package Data;

import Business.DeliveryService;
import Business.MenuItem;
import Business.Order;
import Business.User;

import java.io.*;
import java.util.HashSet;

public class Serial {

    public static void seri(String f1,String f2,String f3) throws IOException {
        System.out.println("Ser");
        System.out.println(DeliveryService.retUser().size());
        System.out.println(DeliveryService.getFoo().size());
        System.out.println(DeliveryService.getAsdk3().size());
        FileOutputStream fileOut =
                new FileOutputStream(f1);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(DeliveryService.retUser());
        out.close();
        fileOut.close();

         fileOut =
                new FileOutputStream(f2);
        out = new ObjectOutputStream(fileOut);
        out.writeObject(DeliveryService.getFoo());
        out.close();
        fileOut.close();


       fileOut =
                new FileOutputStream(f3);
        out = new ObjectOutputStream(fileOut);
        out.writeObject(DeliveryService.getAsdk3());
        out.close();
        fileOut.close();

    }

    public static void des(String f1,String f2,String f3) throws IOException, ClassNotFoundException {
        System.out.println("Des");
        HashSet<User> bla = new HashSet<>();
        HashSet<MenuItem> bla2 = new HashSet<>();
        HashSet<Order> bla3 = new HashSet<>();

        FileInputStream fis = new FileInputStream(f1);
        ObjectInputStream ois = new ObjectInputStream(fis);

        DeliveryService.setAsk((HashSet<User> )ois.readObject());

        ois.close();
        fis.close();

         fis = new FileInputStream(f2);
         ois = new ObjectInputStream(fis);

        DeliveryService.setAsk2((HashSet<MenuItem> )ois.readObject());

        ois.close();
        fis.close();

         fis = new FileInputStream(f3);
         ois = new ObjectInputStream(fis);

        DeliveryService.setAsk3((HashSet<Order> )ois.readObject());

        ois.close();
        fis.close();



    }
}
